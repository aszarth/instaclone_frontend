import React, { Component } from 'react';

import Avatar from '../../assets/avatar.png';

import api from '../../services/api';

import { logout, isAuthenticated, getToken } from '../../services/auth';

class MyAccount extends Component {
  state = {
    username: '',
    email: '',
  };

  async componentDidMount() {
    const token = getToken();
    const response = await api.post('/users/findbytoken', {
      token,
    });
    this.setState({ username: response.data.username });
    this.setState({ email: response.data.mail });
  }

  redirectNonLogged() {
    if (!isAuthenticated()) {
      this.props.history.push('/');
    }
  }

  localLogout() {
    logout();
    window.location.reload(false);
  }

  render() {
    this.redirectNonLogged();
    return (
      <div>
        <p>{this.state.username}</p>
        <p>{this.state.email}</p>
        <img src={Avatar} width="300" height="300" alt="Your avatar" />
        <button type="submit" onClick={this.localLogout}>
          Logout
        </button>
      </div>
    );
  }
}

export default MyAccount;
