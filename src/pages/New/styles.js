import styled from 'styled-components';

// sintaxe do SASS pro auto referenciamento dentro do CSS
export const Form = styled.form`
  width: 100%;
  max-width: 580px;
  margin: 30px auto 0;
  padding: 30px;
  background: #fff;
  border: 1px solid #ddd;

  display: flex;
  flex-direction: column;

  input {
    margin-bottom: 10px;
  }

  input[type='text'] {
    height: 38px;
    border-radius: 4px;
    border: 1px solid #ddd;
    padding: 0 20px;
    font-size: 14px;
  }
`;

// props type do button by function
const getButtonType = (type) => {
  if (type === 'submit') {
    return `
        background: #0099FF;
        color: #FDFDFD;
    `;
  } else if (type === 'back') {
    return `
      background: #FF8080;
      color: #FDFDFD;
    `;
  }
  // default
  return `
    background: #FDFDFD;
    color: #333333;
  `;
};

// button default + props by function
export const Button = styled.button`
  width: 100%;
  padding: 10px 20px;
  border-radius: 4px;
  border: 0;
  background: #7159c1;
  color: #fff;
  font-size: 14px;
  font-weight: bold;
  cursor: pointer;
  ${({type}) => getButtonType(type)}
`;

// 1 herança só pra demonstrar herança msm
// 2 props direto em vez de por função
export const SonButton = styled(Button)`
  background: ${(props) => props.background};
  color: ${(props) => props.color};
`;
