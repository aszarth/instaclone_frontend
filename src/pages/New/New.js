import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import api from '../../services/api';

import { Form, Button, SonButton } from "./styles";

class New extends Component {
    state = {
        image: null,
        author: '',
        place: '',
        description: '',
        hashtags: '',
    };

    // armazenar os valors dos inputs nos states
    // formato de arrow function, pra ter acesso ao this
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    }

    // img tem que tratar de outro jeito, pq vem no formato de array
    handleImageChange = e => {
        this.setState({ image: e.target.files[0] });
    }

    handleSubmit = async e => {
        e.preventDefault(); // previnir comportamento padrao do form de redirecionar
        //console.log(this.state);
        /* se fosse json normal, só texto, era só:
        await api.post('posts', {
            author,
        })
        mas como ta usando aquele formato de img (multipartformdata) .then( */
        const data = new FormData();
        data.append('image', this.state.image);
        data.append('author', this.state.author);
        data.append('place', this.state.place);
        data.append('description', this.state.description);
        data.append('hashtags', this.state.hashtags);
        await api.post('posts', data);

        // toda pagina da aplicação que vem de uma rota
        // como <Route>...
        // vem com essa propriedade
        // historico de navegação do usuário, pra redirecionar
        this.props.history.push('/');
    }

    externalRedirect = (link, ablank = false) => {
        if(ablank === false) { // na msm aba
            window.location.replace(link);
        }
        else if(ablank === true) { // em outra aba
            window.open(
                link,
                '_blank' // <- This is what makes it open in a new window.
            );
        }
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <input type="file" onChange={this.handleImageChange}/>

                    <input
                        type="text"
                        name="author"
                        placeholder="Autor do post"
                        onChange={this.handleChange}
                        value={this.state.author}
                    />
                    <input
                        type="text"
                        name="place"
                        placeholder="Local do post"
                        onChange={this.handleChange}
                        value={this.state.place}
                    />
                    <input
                        type="text"
                        name="description"
                        placeholder="Descrição do post"
                        onChange={this.handleChange}
                        value={this.state.description}
                    />
                    <input
                        type="text"
                        name="hashtags"
                        placeholder="Hashtags do post"
                        onChange={this.handleChange}
                        value={this.state.hashtags}
                    />

                    <Button type="submit">Enviar</Button>
                    <Link to="/">
                        <Button type="back">Voltar</Button>
                    </Link>
                    <SonButton background="#00CC99" color="#FDFDFD" onClick={ () => this.externalRedirect("https://google.com.br/", true) } >Google</SonButton>
                </Form>
            </div>
        );
    }
}

export default New;