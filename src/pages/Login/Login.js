import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import Avatar from '../../assets/avatar.png';

import api from '../../services/api';
import { login, isAuthenticated } from '../../services/auth';

import { Form, Container } from './styles';

class Login extends Component {
  state = {
    mail: '',
    password: '',
    error: '',
  };

  redirectLogged() {
    if (isAuthenticated()) {
      this.props.history.push('/feed');
    }
  }

  handleLogin = async (e) => {
    e.preventDefault();
    const { mail, password } = this.state;
    if (!mail || !password) {
      this.setState({ error: 'Preencha e-mail e senha para continuar!' });
    } else {
      try {
        const response = await api.post('/users/authenticate', {
          mail,
          password,
        });
        login(response.data.token);
        this.props.history.push('/feed');
      } catch (err) {
        this.setState({
          error:
            'Houve um problema com o login, verifique suas credenciais. T.T',
        });
      }
    }
  };

  render() {
    this.redirectLogged();
    return (
      <Container>
        <Form onSubmit={this.handleLogin}>
          <img src={Avatar} alt="Your avatar" />
          {this.state.error && <p>{this.state.error}</p>}
          <input
            type="email"
            placeholder="Endereço de e-mail"
            onChange={(e) => this.setState({ mail: e.target.value })}
          />
          <input
            type="password"
            placeholder="Senha"
            onChange={(e) => this.setState({ password: e.target.value })}
          />
          <button type="submit">Entrar</button>
          <a href="#">Forgotten password</a>
          <hr />
          <Link to="/register">Criar conta grátis</Link>
        </Form>
      </Container>
    );
  }
}

export default withRouter(Login);
