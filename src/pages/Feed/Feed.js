import React, { Component } from 'react';
import api from '../../services/api';
import io from 'socket.io-client';

import { PostList } from './styles';

import more from '../../assets/more.svg';
import like from '../../assets/like.svg';
import comment from '../../assets/comment.svg';
import send from '../../assets/send.svg';

class Feed extends Component {
  state = {
    feed: [],
  };

  async componentDidMount() {
    this.registerToSocket();

    const response = await api.get('posts');

    this.setState({ feed: response.data });
  }

  registerToSocket = () => {
    const socket = io('http://localhost:3333');

    // post, like
    socket.on('post', (newPost) => [
      // criando um novo feed, com o post que recebeu em realtime
      // e copiando o que já tinha do post
      this.setState({ feed: [newPost, ...this.state.feed] }),
    ]);

    socket.on('like', (likedPost) => {
      // procurar o post com o msm id que o recebido no socket e alterar os likes
      this.setState({
        // percorrer o feed utilizando map
        feed: this.state.feed.map((post) =>
          // se o id post q estou percorrendo nesse momento
          // for igual ao id do post que recebeu um novo curtir
          // atualizar as informações desse novo post com o likePost se nao, manda como ele ja esta no estado
          post.id === likedPost.id ? likedPost : post
        ),
      });
    });
  };

  handleLike = (id) => {
    api.post(`/posts/${id}/like`);
  };

  render() {
    return (
      <PostList>
        {this.state.feed.map((post) => (
          /*
              toda vez que for fazer um map pra percorrer um vetor
              tem que adicionar a propriedade key que vem no primeiro elemento dps do map
              com uma chave unica

              isso faz o react encontra mais facil os elementos que foram
              adicionados,alterados ou deletados
              ou adicionados dentro da arvore da dom
          */
          <article key={post.id}>
            <header>
              <div className="user-info">
                <span>{post.author}</span>
                <span className="place">{post.place}</span>
              </div>
              <img src={more} alt="Mais" />
            </header>
            <img src={`http://localhost:3333/files/${post.image}`} alt="" />
            <footer>
              <div className="actions">
                {/* () => arrow function pra passar a função como referencia para o onclick, se fosse sem ia só executar o codigo, é pra passar uma função como referencia */}
                <button type="button" onClick={() => this.handleLike(post.id)}>
                  <img src={like} alt="" />
                </button>
                <img src={comment} alt="" />
                <img src={send} alt="" />
              </div>
              <strong>{post.likes} curtidas</strong>
              <p>
                {post.description}
                <span>{post.hashtags}</span>
              </p>
            </footer>
          </article>
        ))}
      </PostList>
    );
  }
}

export default Feed;
