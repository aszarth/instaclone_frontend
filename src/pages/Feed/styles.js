import styled from 'styled-components';

export const PostList = styled.div`
  width: 100%;
  max-width: 580px;
  margin: 0 auto;
  padding: 0 30px; /* se largura for pequena deixar um espaçinho na esq/dir */

  article {
    background: #fff;
    border: 1px solid #ddd;
    margin-top: 30px;
  }

  article header {
    padding: 20px;

    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  article header .user-info {
    display: flex;
    flex-direction: column;
  }

  /* nome do usuario */
  article header .user-info span {
    font-size: 13px;
  }

  /* localizacao do usuario */
  article header .user-info span.place {
    font-size: 11px;
    color: #666;
    margin-top: 3px;
  }

  /* só a que vem diretamente do article */
  article > img {
    width: 100%;
  }

  /* footer */
  article footer {
    padding: 20px;
  }

  article footer .actions {
    margin-bottom: 10px;
  }

  /* button ficar inv, sem aquela bugada na estilização, ficar só a img */
  article footer .actions button {
    background: transparent;
    border: 0;
    cursor: pointer;
  }

  article footer .actions img {
    height: 20px;
    margin-right: 10px;
  }

  article footer p {
    font-size: 13px;
    margin-top: 2px;
    line-height: 18px;
  }

  /* hashtags */
  article footer p span {
    color: #7159c1;
    display: block;
  }
`;
