import React from 'react';
import {Switch, Route, Redirect, BrowserRouter} from 'react-router-dom';

import Header from './components/Header';

import {isAuthenticated} from './services/auth';

import Feed from './pages/Feed/Feed.js';
import New from './pages/New/New.js';
import Register from './pages/Register/Register.js';
import Login from './pages/Login/Login.js';
import NotFound from './pages/Login/Login.js';
import MyAccount from './pages/MyAccount/MyAccount.js';

const RouteNavHeader = ({component: Component, ...rest}) => (
  <Route
    {...rest}
    render={(props) => (
      <>
        <Header />
        <Component {...props} />
      </>
    )}
  />
);

const PrivateRoute = ({component: Component, navheader, ...rest}) => (
  <Route
    {...rest}
    render={(props) => {
      if (isAuthenticated()) {
        if (!navheader) {
          return <Component {...props} />;
        } else {
          return <RouteNavHeader component={Component} {...props} />;
        }
      } else {
        return (
          <Redirect to={{pathname: '/', state: {from: props.location}}} />
        );
      }
    }}
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/register" component={Register} />
      <PrivateRoute path="/feed" component={Feed} navheader={true} />
      <PrivateRoute path="/new" component={New} navheader={true} />
      <PrivateRoute path="/myaccount" component={MyAccount} navheader={true} />
      <RouteNavHeader path="*" component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
