import React from 'react';
import {Link} from 'react-router-dom';

import {NavHeader} from './styles.js';

import logo from '../assets/logo.svg';
import camera from '../assets/camera.svg';

const FontAwesome = require('react-fontawesome');

export default function Header() {
  return (
    <NavHeader>
      <div className="navheader_content">
        <Link to="/feed">
          <img src={logo} alt="InstaRocket" />
        </Link>
        <Link to="/new">
          <img src={camera} alt="Enviar Publicacao" />
        </Link>
        <Link to="/myaccount">
          <FontAwesome
            name="user"
            size="2x"
            style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
          />
        </Link>
      </div>
    </NavHeader>
  );
}
