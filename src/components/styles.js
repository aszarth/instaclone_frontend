import styled from 'styled-components';

// sintaxe do SASS pro auto referenciamento dentro do CSS
export const NavHeader = styled.header`
  background: #fff;
  height: 72px;
  border-bottom: 1px solid #ddd;

  .navheader_content {
    width: 100%;
    max-width: 980px; /* largura dps pq assim se tiver -980, utilizar 100% */
    margin: 0 auto; /* centralizar */
    height: 72px;
    padding: 0 30px;

    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
