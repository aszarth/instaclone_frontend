#InstaClone FrontEND

![img 1](https://gitlab.com/aszarth/instaclone_frontend/raw/master/showoff/Screenshot%20from%202019-07-01%2001-17-44.png)
![img 2](https://gitlab.com/aszarth/instaclone_frontend/raw/master/showoff/Screenshot%20from%202019-06-29%2020-15-26.png)

FrontEnd WEB clone do instagram

Consultando dados de uma API Rest node (https://gitlab.com/aszarth/instaclone_backend)

Com essa aplicação é possível:

- postar fotos
- ver posts, novos posts são atualizados em tempo real
- dar link e atualizar numeros de likes em tempo real

estilização feita com Styled Components

padronização do código com ESLint com padrões google
integrado com o plugin prettier do vscode

versão de distribuição:
npm run build, pasta dist

informações sobre a arquitetura do sistema em:
diario_arquitetura

informações sobre dependencias baixadas e como rodar o projeto em:
diario_npm

--- INFO ---
.env
[code]
REACT_APP_API_URL=http://localhost:3333
[/code]
